function getClientes(){
  const url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status ==200){
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET",url,true);
  request.send();
}

function procesarClientes(){
  var JSONCliente = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  const urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONCliente.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONCliente.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONCliente.value[i].City;

    var columnaPais = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if(JSONCliente.value[i].Country == "UK"){
      imgBandera.src = urlBandera + "United-Kingdom.png";
    }else{
      imgBandera.src = urlBandera + JSONCliente.value[i].Country+".png";
    }

    columnaPais.innerText = JSONCliente.value[i].Country;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(imgBandera);

    tbody.appendChild(nuevaFila);

  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
